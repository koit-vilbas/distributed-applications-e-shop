<?php
namespace KoitVilbas\ShoppingCart;

require_once __DIR__ . '/../vendor/autoload.php';

use Dotenv\Dotenv;
use KoitVilbas\ShoppingCart\Service;

$dotenv = Dotenv::create(__DIR__ . '/../');
$dotenv->load();

$entityManager = Service::Instance();