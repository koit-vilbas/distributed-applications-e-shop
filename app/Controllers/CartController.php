<?php
namespace KoitVilbas\ShoppingCart\Controllers;
use Klein\App;
use Klein\Request;
use Klein\Response;
use KoitVilbas\ShoppingCart\Models\Product;
use KoitVilbas\ShoppingCart\Service;
use Symfony\Component\HttpFoundation\Session\Session;

class CartController {
    private $em;
    private $session;

    public function __construct() {
        $this->em = Service::Instance();
        $this->session = new Session();
    }

    public function store(Request $request, Response $response, $service, App $app)
    {
        $id = $request->param('product_id', '');
        $basket = $this->session->get('basket');
        $basket[] = (int)$id;
        $this->session->set('basket', $basket);

        return $response->redirect('/')->send();
    }

    public function basket(Request $request, Response $response, $service, App $app) {
        $productIDs = $this->session->get('basket', []);

        $qb = $this->em->createQueryBuilder();
        $query = $qb->select('p')
            ->from('KoitVilbas\ShoppingCart\Models\Product', 'p')
            ->where($qb->expr()->in('p.id', ':ids'))
            ->orderBy('p.title', 'ASC')
            ->setParameter('ids', array_unique($productIDs))
            ->getQuery();

        $products = $query->execute();

        //Map products by ID
        $mappedProducts = [];
        foreach ($products as $product) {
            /** @var $product Product */

            $mappedProducts[$product->getId()] = $product;
        }

        foreach ($productIDs as $productID) {
            if(array_key_exists($productID, $mappedProducts )) {
                $mappedProducts[$productID]->quantity++;
            };
        }

        $totalSum = 0;
        foreach ($mappedProducts as $product) {
            /** @var $product Product */
            $totalSum += $product->getPrice() * $product->quantity;
        }

        $userKey = openssl_pkey_get_private(base64_decode(getenv('USER_KEY')));
        $bankCert = base64_decode(getenv('BANK_CERT'));

        $currentDateTime = new \DateTime();

        $fields = [
            "VK_SERVICE"     => "1011",
            "VK_VERSION"     => "008",
            "VK_SND_ID"      => "uid100010",
            "VK_STAMP"       => "12345",
            "VK_AMOUNT"      => $totalSum,
            "VK_CURR"        => "EUR",
            "VK_ACC"         => "EE152200221234567897",
            "VK_NAME"        => "Shopping Cart OÜ",
            "VK_REF"         => "1234561",
            "VK_LANG"        => "EST",
            "VK_MSG"         => "Order no. " . rand(1000, 9999),
            "VK_RETURN"      => getenv('APP_URL') . 'payment/success',
            "VK_CANCEL"      => getenv('APP_URL') . 'payment/cancel',
            "VK_DATETIME"    => $currentDateTime->format(\DateTime::ISO8601),
            "VK_ENCODING"    => "utf-8",
        ];

        $data = str_pad (mb_strlen($fields["VK_SERVICE"], "UTF-8"), 3, "0", STR_PAD_LEFT) . $fields["VK_SERVICE"] .
            str_pad (mb_strlen($fields["VK_VERSION"], "UTF-8"), 3, "0", STR_PAD_LEFT) . $fields["VK_VERSION"] .
            str_pad (mb_strlen($fields["VK_SND_ID"], "UTF-8"),  3, "0", STR_PAD_LEFT) . $fields["VK_SND_ID"] .
            str_pad (mb_strlen($fields["VK_STAMP"], "UTF-8"),   3, "0", STR_PAD_LEFT) . $fields["VK_STAMP"] .
            str_pad (mb_strlen($fields["VK_AMOUNT"], "UTF-8"),  3, "0", STR_PAD_LEFT) . $fields["VK_AMOUNT"] .
            str_pad (mb_strlen($fields["VK_CURR"], "UTF-8"),    3, "0", STR_PAD_LEFT) . $fields["VK_CURR"] .
            str_pad (mb_strlen($fields["VK_ACC"], "UTF-8"),     3, "0", STR_PAD_LEFT) . $fields["VK_ACC"] .
            str_pad (mb_strlen($fields["VK_NAME"], "UTF-8"),    3, "0", STR_PAD_LEFT) . $fields["VK_NAME"] .
            str_pad (mb_strlen($fields["VK_REF"], "UTF-8"),     3, "0", STR_PAD_LEFT) . $fields["VK_REF"] .
            str_pad (mb_strlen($fields["VK_MSG"], "UTF-8"),     3, "0", STR_PAD_LEFT) . $fields["VK_MSG"] .
            str_pad (mb_strlen($fields["VK_RETURN"], "UTF-8"),  3, "0", STR_PAD_LEFT) . $fields["VK_RETURN"] .
            str_pad (mb_strlen($fields["VK_CANCEL"], "UTF-8"),  3, "0", STR_PAD_LEFT) . $fields["VK_CANCEL"] .
            str_pad (mb_strlen($fields["VK_DATETIME"], "UTF-8"), 3, "0", STR_PAD_LEFT) . $fields["VK_DATETIME"];

        openssl_sign ($data, $signature, $userKey, OPENSSL_ALGO_SHA1);

        $fields["VK_MAC"] = base64_encode($signature);

        return $app->twig->render('cart/basket.twig',
            ['basket' => $this->session->get('basket'), 'products' => $mappedProducts, 'total' => $totalSum, 'paymentFields' => $fields]);
    }

    public function success(Request $request, Response $response, $service, App $app) {
        $bankCert = openssl_pkey_get_public(base64_decode(getenv('BANK_CERT')));

        $fields = array(
            "VK_SERVICE"     => $request->param('VK_SERVICE'),
            "VK_VERSION"     => $request->param('VK_VERSION'),
            "VK_SND_ID"     => $request->param('VK_SND_ID'),
            "VK_REC_ID"     => $request->param('VK_REC_ID'),
            "VK_STAMP"     => $request->param('VK_STAMP'),
            "VK_T_NO"     => $request->param('VK_T_NO'),
            "VK_AMOUNT"     => $request->param('VK_AMOUNT'),
            "VK_CURR"     => $request->param('VK_CURR'),
            "VK_REC_ACC"     => $request->param('VK_REC_ACC'),
            "VK_REC_NAME"     => $request->param('VK_REC_NAME'),
            "VK_SND_ACC"     => $request->param('VK_SND_ACC'),
            "VK_SND_NAME"     => $request->param('VK_SND_NAME'),
            "VK_REF"     => $request->param('VK_REF'),
            "VK_MSG"     => $request->param('VK_MSG'),
            "VK_T_DATETIME"     => $request->param('VK_T_DATETIME'),
            "VK_ENCODING"     => $request->param('VK_ENCODING'),
            "VK_LANG"     => $request->param('VK_LANG'),
            "VK_MAC"     => $request->param('VK_MAC'),
            "VK_AUTO"     => $request->param('VK_AUTO')
        );

        $data = str_pad (mb_strlen($fields["VK_SERVICE"], "UTF-8"),   3, "0", STR_PAD_LEFT) . $fields["VK_SERVICE"] .
            str_pad (mb_strlen($fields["VK_VERSION"], "UTF-8"),   3, "0", STR_PAD_LEFT) . $fields["VK_VERSION"] .
            str_pad (mb_strlen($fields["VK_SND_ID"], "UTF-8"),    3, "0", STR_PAD_LEFT) . $fields["VK_SND_ID"] .
            str_pad (mb_strlen($fields["VK_REC_ID"], "UTF-8"),    3, "0", STR_PAD_LEFT) . $fields["VK_REC_ID"] .
            str_pad (mb_strlen($fields["VK_STAMP"], "UTF-8"),     3, "0", STR_PAD_LEFT) . $fields["VK_STAMP"] .
            str_pad (mb_strlen($fields["VK_T_NO"], "UTF-8"),      3, "0", STR_PAD_LEFT) . $fields["VK_T_NO"] .
            str_pad (mb_strlen($fields["VK_AMOUNT"], "UTF-8"),    3, "0", STR_PAD_LEFT) . $fields["VK_AMOUNT"] .
            str_pad (mb_strlen($fields["VK_CURR"], "UTF-8"),      3, "0", STR_PAD_LEFT) . $fields["VK_CURR"] .
            str_pad (mb_strlen($fields["VK_REC_ACC"], "UTF-8"),   3, "0", STR_PAD_LEFT) . $fields["VK_REC_ACC"] .
            str_pad (mb_strlen($fields["VK_REC_NAME"], "UTF-8"),  3, "0", STR_PAD_LEFT) . $fields["VK_REC_NAME"] .
            str_pad (mb_strlen($fields["VK_SND_ACC"], "UTF-8"),   3, "0", STR_PAD_LEFT) . $fields["VK_SND_ACC"] .
            str_pad (mb_strlen($fields["VK_SND_NAME"], "UTF-8"),  3, "0", STR_PAD_LEFT) . $fields["VK_SND_NAME"] .
            str_pad (mb_strlen($fields["VK_REF"], "UTF-8"),       3, "0", STR_PAD_LEFT) . $fields["VK_REF"] .
            str_pad (mb_strlen($fields["VK_MSG"], "UTF-8"),       3, "0", STR_PAD_LEFT) . $fields["VK_MSG"] .
            str_pad (mb_strlen($fields["VK_T_DATETIME"], "UTF-8"), 3, "0", STR_PAD_LEFT) . $fields["VK_T_DATETIME"];

        if (openssl_verify ($data, base64_decode($fields["VK_MAC"]), $bankCert) !== 1) {
            $signatureVerified = false;
        }else{
            $signatureVerified = true;
            $this->session->clear();
        }

        return $app->twig->render('cart/success.twig', ['verified' => $signatureVerified]);
    }

    public function cancel(Request $request, Response $response, $service, App $app) {
        $this->session->clear();
        return $app->twig->render('cart/cancel.twig');
    }
}