<?php

namespace KoitVilbas\ShoppingCart\Controllers;


use KoitVilbas\ShoppingCart\Service;
use Symfony\Component\HttpFoundation\Session\Session;

class SiteController {
    private $em;
    private $session;

    public function __construct() {
        $this->em = Service::Instance();
        $this->session = new Session();
    }

    public function index($req, $res, $service, $app) {
        $aProducts = [];
        if ($products = $this->em->getRepository('KoitVilbas\ShoppingCart\Models\Product')->findAll()) {
            foreach ($products as $product) {
                $aProducts[] = $product->toArray();
            }
        }

        return $app->twig->render('cart/index.twig', ['products' => $aProducts, 'basket' => $this->session->get('basket')]);
    }
}