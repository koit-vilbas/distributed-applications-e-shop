<?php
/**
 * Created by PhpStorm.
 * User: koit.vilbas
 * Date: 02.05.2019
 * Time: 13:26
 */

namespace KoitVilbas\ShoppingCart\Models;

/**
 * @Entity
 * @Table(name="products")
 */
class Product {
    public $quantity = 0;

    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    private $id;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @Column(type="string")
     */
    private $title;

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @Column(type="float", precision=2, scale=18 )
     */
    private $price;

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @Column(type="text")
     */
    private $description;

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @Column(type="string")
     */
    private $imageURL;

    /**
     * @return string
     */
    public function getImageURL()
    {
        return $this->imageURL;
    }

    /**
     * @param string $imageURL
     */
    public function setImageURL($imageURL)
    {
        $this->imageURL = $imageURL;
    }

    public function toJSON() {
        return json_encode([
            'title' => $this->getTitle(),
            'price' => $this->getPrice(),
            'description' => $this->getDescription(),
            'image_url' => $this->imageURL(),
        ]);
    }

    public function toArray() {
        return [
            'id' => $this->getId(),
            'title' => $this->getTitle(),
            'price' => $this->getPrice(),
            'description' => $this->getDescription(),
            'image_url' => $this->getImageURL()
        ];
    }

}