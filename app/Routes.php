<?php
use \Klein\Klein;

use KoitVilbas\ShoppingCart\Controllers\CartController;
use \KoitVilbas\ShoppingCart\Controllers\SiteController;

$router = new Klein();
$siteCtrl = new SiteController();

$router->respond(function ($request, $response, $service, $app) use ($router) {
    $app->register('twig', function () {
        $loader = new \Twig\Loader\FilesystemLoader(__DIR__ . '/../views');
        return new \Twig\Environment($loader);
    });
});

$router->respond('GET', ROOT_URL, [$siteCtrl, 'index']);
$router->respond('GET', ROOT_URL . 'basket', [new CartController(), 'basket']);
$router->respond('POST', ROOT_URL . 'payment/success', [new CartController(), 'success']);
$router->respond('POST', ROOT_URL . 'payment/cancel', [new CartController(), 'cancel']);
$router->respond('POST', ROOT_URL, [new CartController(), 'store']);

$router->dispatch();