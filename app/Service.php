<?php

namespace KoitVilbas\ShoppingCart;

use \Doctrine\ORM\Tools\Setup;
use \Doctrine\ORM\EntityManager;
use \Doctrine\ORM\ORMException;


final class Service {
    private $dbParams;
    private $config;

    private function __construct() {
        $paths = [dirname(__FILE__)."/Models"];

        $isDevMode = true;

        $this->dbParams = [
            'driver' => 'pdo_mysql',
            'user' => getenv('DB_USER'),
            'password' => getenv('DB_PASSWORD'),
            'dbname' => getenv('DB_NAME'),
            'host' => getenv('DB_HOST'),
            'port' => getenv('DB_PORT')
        ];

        $this->config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode);
    }

    public static function Instance() {
        static $inst = null;

        if ($inst === null) {
            $service = new Service();
            try {
                $inst = EntityManager::create($service->dbParams, $service->config);
            } catch (ORMException $ORMException) {
                return null;
            }
        }
        return $inst;
    }
}