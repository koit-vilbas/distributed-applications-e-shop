<?php

use Dotenv\Dotenv;

define('ROOT_URL', '/');

require_once  __DIR__ . '/../app/Bootstrap.php';
require_once  __DIR__ . '/../app/Routes.php';

error_reporting(E_ALL);
ini_set('display_errors', 'On');
