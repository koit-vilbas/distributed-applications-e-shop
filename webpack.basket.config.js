const path = require('path');
const webpack = require('webpack');

module.exports = {
    entry: "./public/assets/src/js/basket.js", // Entry File
    devtool: 'source-map',
    output: {
        path: path.resolve(__dirname, "public/assets/dist"), //Output Directory
        filename: "basket.js", //Output file
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                query: {
                    presets: ['@babel/env']
                }
            },
            {
                test: /\.css$/,
                use: [{
                    loader: "style-loader"
                }, {
                    loader: "css-loader"
                }]
            },
            {
                test: /\.scss$/,
                use: [{
                    loader: "style-loader"
                }, {
                    loader: "css-loader"
                }, {
                    loader: "sass-loader",
                    options: {
                        includePaths: [require('node-bourbon').includePaths]
                    }
                }]
            },
            {
                test: /\.(png|jpg|gif|svg)$/i,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            limit: 8192,
                        },
                    },
                ],
            },
        ]
    }
};
